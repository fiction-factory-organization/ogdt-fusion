# OGDT Fusion

**OpenSource** application under **GPL-3.0-or-later**. Designed for OGDT app. Ressources manager and Versionning module.

## Technicals details


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

1. Make sure you have nodejs, docker and docker-compose installed.

### Installing

1. Execute a `git clone` to get a copy of the sources.

2. For dev switch to branch "dev", to deploy the app switch to branch "stable".

3. navigate into "app" dir and install dependencies: `cd app && npm install`

4. Build and run dev environment: `docker-compose build && docker-compose up -d`

5. Run fusion server: `node index.js`

6. With fusion server running: `cd app/utils && node init-fs.js`. You should obtain:

```
[INFO] : Creating Root Directory / ...
[INFO] : Root directory registered!
[INFO] : File system successfully initialized! Exiting...
```

If you already used fusion in a docker environment, use `node init-fs.js --force-reset` instead.

Your fusion server is running and ready to serve.

7. (Optional) From project root, you can access command line with `cd app/utils && node term.js`

### Running the tests

*Comming later!*

## Deployment / production server installation

*Comming later!*

## Built With

* nodejs - Backend JS framework
* npm - Dependency Management
* Docker - Containers
