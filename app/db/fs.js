/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-06-29T09:34:35+02:00
 * @Project: OGDT Fusion
 * @Filename: fs.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-10T12:41:17+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */



let fileModel = require('../models/file');
let fileRefModel = require('../models/fileRef');
let dirModel = require('../models/directory');
let contextNodeModel = require('../models/contextNode');
let fileNodeModel = require('../models/fileNode');
let deltaModel = require('../models/delta');

let FileSystem = class {

    constructor(payload) {
      this.payload = payload;
    }

    // CREATE METHODS

    createFile(cb) {
      new fileModel(this.payload).save(cb);
    }

    createFileRef(cb) {
      new fileRefModel(this.payload).save(cb);
    }

    createDir(cb) {
      new dirModel(this.payload).save(cb);
    }

    createContextNode(cb) {
      new contextNodeModel(this.payload).save(cb);
    }

    createFileNode(cb) {
      new fileNodeModel(this.payload).save(cb);
    }

    createDelta(cb) {
      new deltaModel(this.payload).save(cb);
    }

    // GET METHODS

    getFile(cb) {
      const criteria = this.payload.criteria;
      const projections = this.payload.projections;
      const options = this.payload.options;
      return fileModel.find(criteria, projections, options, cb)
    }

    getFileRef(cb) {
      const criteria = this.payload.criteria;
      const projections = this.payload.projections;
      const options = this.payload.options;
      return fileRefModel.find(criteria, projections, options, cb)
    }

    getDir(cb) {
      const criteria = this.payload.criteria;
      const projections = this.payload.projections;
      const options = this.payload.options;
      return dirModel.find(criteria, projections, options, cb);
    }

    getContextNode(cb) {
      const criteria = this.payload.criteria;
      const projections = this.payload.projections;
      const options = this.payload.options;
      return contextNodeModel.find(criteria, projections, options, cb)
    }

    // DELETE METHODS

    deleteFile(cb) {
      const criteria = this.payload.criteria;
      const projections = this.payload.projections;
      const options = this.payload.options;
      return fileModel.find(criteria, projections, options, cb).deleteMany(cb).exec();
    }

    deleteFileRef(cb) {
      const criteria = this.payload.criteria;
      const projections = this.payload.projections;
      const options = this.payload.options;
      return fileRefModel.find(criteria, projections, options, cb).deleteMany(cb).exec();
    }

    deleteDir(cb) {
      const criteria = this.payload.criteria;
      const projections = this.payload.projections;
      const options = this.payload.options;
      return dirModel.find(criteria, projections, options, cb).deleteMany(cb).exec();
    }

    deleteNode(cb) {
      const criteria = this.payload.criteria;
      const projections = this.payload.projections;
      const options = this.payload.options;
      return nodeModel.find(criteria, projections, options, cb).deleteMany(cb).exec();
    }

    resetHierarchy(cb) {
      return dirModel.deleteMany({}, cb);
    }

    // UPDATE METHODS
    updateDir(cb) {
      dirModel.updateOne({ _id: this.payload._id}, this.payload, {}, cb);
    }

    updateFileRef(cb) {
      fileRefModel.updateOne({ _id: this.payload._id}, this.payload, {}, cb);
    }
};
module.exports = {
  FileSystem: FileSystem,
};
