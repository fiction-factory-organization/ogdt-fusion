/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-08-05T16:32:38+02:00
 * @Project: OGDT
 * @Filename: fileSystemServer.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-05T17:39:20+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

// Consts

global.DEFAULT_PERMISSIONS = 'rwxr-----';
global.DEFAULT_OWNER = 'fusion';
global.DEFAULT_GROUP = 'fusion';

// methods

const ls = require('./fileSystemServer/ls');
const cd = require('./fileSystemServer/cd');
const mkdir = require('./fileSystemServer/mkdir');
const mv = require('./fileSystemServer/mv');
const rm = require('./fileSystemServer/rm');
const rmdir = require('./fileSystemServer/rmdir');
const touch = require('./fileSystemServer/touch');

module.exports = {
  ls: ls,
  cd: cd,
  mkdir: mkdir,
  mv: mv,
  rm: rm,
  rmdir: rmdir,
  touch: touch,
}
