/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-06-29T09:34:35+02:00
 * @Project: OGDT Fusion
 * @Filename: logger.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-07-10T02:42:06+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */



const LOG_INFO = 1;
const LOG_WARNING = 2;
const LOG_ERROR = 3;

const RESET_COLOR = '\x1b[0m';
const INFO_COLOR = '\x1b[0m';
const WARNING_COLOR = '\x1b[33m';
const ERROR_COLOR = '\x1b[31m';

function logMessage(type, message){
  switch(type){
    case LOG_INFO:
      message = '[INFO] : '+message;
      console.log(INFO_COLOR+'%s'+RESET_COLOR, message);
      break;
    case LOG_WARNING:
      message = '[WARNING] : '+message;
      console.log(WARNING_COLOR+'%s'+RESET_COLOR, message);
      break;
    case LOG_ERROR:
      message = '[ERROR] : '+message;
      console.log(ERROR_COLOR+'%s'+RESET_COLOR, message);
      break;
  }
}

module.exports = {
    logMessage: logMessage,
    LOG_INFO: LOG_INFO,
    LOG_WARNING: LOG_WARNING,
    LOG_ERROR: LOG_ERROR,
};
