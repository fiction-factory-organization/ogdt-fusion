/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-06-29T09:34:35+02:00
 * @Project: OGDT Fusion
 * @Filename: term.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-10T13:37:25+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */



const Logger = require('../commons/logger');
const Client = require('./client');

// Consts
const CONST = require('./const');
const TERMINAL = CONST.TERMINAL;
const FS = CONST.FS;
const DATES = CONST.DATES;

const User = 'fusion';

// Get process.stdin as the standard input object.
var stdin = process.stdin;
// Set input character encoding.
stdin.setEncoding('utf-8');

init = false;

// Set default working dir
global.wd = {}
global.wd.id = undefined;
global.wd.path = ['/'];

(async () => {
    try {
      await Client.rpc.Cd(
        { path: global.wd.path, wd: global.wd.id },
        (error, result) => {
          if (!error) {
            global.wd.id = result.wd;
            global.wd.path = result.path;
            init = true;
          } else {
            Logger.logMessage(Logger.LOG_ERROR,error);
            Logger.logMessage(Logger.LOG_ERROR,'Unable to retrieve Working directory. Exiting...');
            process.exit();
          }
          // First prompt
          displayPrompt();
      });
    } catch (e) {
      Logger.logMessage(Logger.LOG_ERROR,e);
      Logger.logMessage(Logger.LOG_ERROR,'Unknown error. Exiting...');
      process.exit();
    }
})();

// User text input event
stdin.on('data', function (data) {
    if (init == false) {
      process.exit();
    }
    data = check(data)
    if (data == false){
      displayPrompt();
      return;
    }
    run(data);
});

process.on('SIGINT', () => {
    if (init == false) {
      process.exit();
    }
    process.stdout.write("\n");
    displayPrompt();
});

function check(data)
{
  data = splitInstruction(data);

  switch (data['command']) {
    case 'exit':
      if (data['args'].length != 0){
        return false;
      }
      Logger.logMessage(Logger.LOG_INFO,'exiting...');
      process.exit();
      break;
    case 'cd':
      if (data['args'].length < 1) {
        Logger.logMessage(Logger.LOG_INFO,TERMINAL.CD_MESSAGES['TFA']);
        return false;
      }
      if (data['args'].length > 1) {
        Logger.logMessage(Logger.LOG_WARNING,TERMINAL.MESSAGES['TMA']);
        return false;
      }
      return data;
    case 'ls':
      return data;
    case 'touch':
      if (data['args'].length < 1) {
        Logger.logMessage(Logger.LOG_INFO,TERMINAL.TOUCH_MESSAGES['TFA']);
        return false;
      }
      if (data['args'].length > 1) {
        Logger.logMessage(Logger.LOG_WARNING,TERMINAL.MESSAGES['TMA'])
        return false;
      }
      return data;
    case 'mkdir':
      if (data['args'].length < 1) {
        Logger.logMessage(Logger.LOG_INFO,TERMINAL.MKDIR_MESSAGES['TFA']);
        return false;
      }
      if (data['args'].length > 1){
        Logger.logMessage(Logger.LOG_WARNING,TERMINAL.MESSAGES['TMA'])
        return false;
      }
      return data;
    case 'rmdir':
      if (data['args'].length < 1) {
        Logger.logMessage(Logger.LOG_INFO,TERMINAL.RMDIR_MESSAGES['TFA']);
        return false;
      }
      if (data['args'].length > 1){
        Logger.logMessage(Logger.LOG_WARNING,TERMINAL.MESSAGES['TMA'])
        return false;
      }
      return data;
    case 'rm':
      if (data['args'].length < 1) {
        Logger.logMessage(Logger.LOG_INFO,TERMINAL.RM_MESSAGES['TFA']);
        return false;
      }
      if (data['args'].length > 1){
        Logger.logMessage(Logger.LOG_WARNING,TERMINAL.MESSAGES['TMA'])
        return false;
      }
      return data;
    case 'help':
      return data;
    case 'mv':
      if (data['args'].length < 2) {
        Logger.logMessage(Logger.LOG_INFO,TERMINAL.MV_MESSAGES['TFA']);
        return false;
      }
      if (data['args'].length > 2){
        Logger.logMessage(Logger.LOG_WARNING,TERMINAL.MESSAGES['TMA'])
        return false;
      }
      return data;
    case '':
      return false;
    default:
      Logger.logMessage(Logger.LOG_ERROR,TERMINAL.MESSAGES['UNKNOWN'])
      return false;
  }
}

function run(data)
{
  switch (data['command']) {
    case 'ls':
      console.log(data);
      // List current dir content
      if (!data['args'][0]) {
        path = ['.'];
      } else {
        path = buildPath(data['args'][0]);
      }
      if (path == false) {
        displayPrompt();
        break;
      }
      Client.rpc.ls({path: path, wd: global.wd.id}, (error, docs) => {
          if (!error) {
            displayList(docs);
          } else {
            Logger.logMessage(Logger.LOG_ERROR,error);
          }
          displayPrompt();
      });
      break;
    case 'cd':
      // Go into a specified directory
      path = buildPath(data['args'][0]);
      if (path == false) {
        displayPrompt();
        break;
      }
      Client.rpc.cd(
        { path: path, wd: global.wd.id },
        (error, result) => {
          if (!error) {
              global.wd.id = result.wd;
              global.wd.path = result.path;
          } else {
              Logger.logMessage(Logger.LOG_ERROR,error);
          }
          displayPrompt();
      });
      break;
    case 'mkdir':
      // Create a new directory
      let dir = { name: '', path: [], wd: global.wd.id, owner: 'fusion', group: 'fusion', permissions: '' };
      path = buildPath(data['args'][0]);
      if (path == false) {
        displayPrompt();
        break;
      }
      dir.name = path.pop();
      dir.path = path;
      Client.rpc.mkdir(dir, (error, result) => {
          if (!error) {
              console.log(result);
              Logger.logMessage(Logger.LOG_INFO,result.message);
          } else {
              Logger.logMessage(Logger.LOG_ERROR,error);
          }
          displayPrompt();
      });
      break;
    case 'touch':
      // Create a new file
      let file = { name: '', path: [], wd: global.wd.id, owner: 'fusion', group: 'fusion', permissions: '' };
      path = buildPath(data['args'][0]);
      if (path == false) {
        displayPrompt();
        break;
      }
      fileFullName = path.pop().split('.');
      if (fileFullName.length != 2) {
        Logger.logMessage(Logger.LOG_WARNING,'File extension is invalid');
        displayPrompt();
        break;
      }
      file.name = fileFullName[0];
      file.type = fileFullName[1];
      file.path = path;
      Client.rpc.touch(file, (error, result) => {
        if (!error) {
            Logger.logMessage(Logger.LOG_INFO,result.message);
        } else {
            Logger.logMessage(Logger.LOG_ERROR,error);
        }
          displayPrompt();
      });
      break;
    case 'rmdir':
      // Remove a dir
      path = buildPath(data['args'][0]);
      if (path == false) {
        displayPrompt();
        break;
      }
      Client.rpc.rmdir(
        { path: path, wd: global.wd.id },
        (error) => {
          if (!error) {
              Logger.logMessage(Logger.LOG_INFO,"Directory successfully deleted");
          } else {
              Logger.logMessage(Logger.LOG_ERROR,error);
          }
          displayPrompt();
      });
      break;
    case 'rm':
      // Remove a file
      path = buildPath(data['args'][0]);
      if (path == false) {
        displayPrompt();
        break;
      }
      fileFullName = path.pop().split('.');
      if (fileFullName.length != 2) {
        Logger.logMessage(Logger.LOG_WARNING,'File extension is invalid');
        displayPrompt();
        break;
      }
      Client.rpc.rm(
        { path: path, wd: global.wd.id, name: fileFullName[0], type: fileFullName[1] },
        (error, result) => {
          if (!error) {
              Logger.logMessage(Logger.LOG_INFO,result.message);
          } else {
              Logger.logMessage(Logger.LOG_ERROR,error);
          }
          displayPrompt();
      });
      break;
    case 'mv':
      path = buildPath(data['args'][0]);
      newPath = buildPath(data['args'][1]);
      if (path == false || newPath ==  false) {
        displayPrompt();
        break;
      }
      fileFullName = path.pop().split('.');
      if (newPath[newPath.length-1] != '..' && newPath[newPath.length-1] != '.') {
          newFileFullName = newPath.pop().split('.');
      } else {
          newFileFullName = fileFullName;
      }
      if (fileFullName.length == 2 && (newFileFullName.length == 2 || newPath[newPath.length-1] == '..')) {
        message = {
          files: [
            { path: path, wd: global.wd.id, name: fileFullName[0], type: fileFullName[1] },
            { path: newPath, wd: global.wd.id, name: newFileFullName[0], type: newFileFullName[1] }
          ],
          fileType: FS.FILE
        };
      } else if (fileFullName.length == 1 && (newFileFullName.length == 1 || newPath[newPath.length-1] == '..')) {
        path.push(fileFullName[0]);
        newPath.push(newFileFullName[0]);
        message = {
          files: [
            { path: path, wd: global.wd.id, name: fileFullName[0], type: fileFullName[1] },
            { path: newPath, wd: global.wd.id, name: newFileFullName[0], type: newFileFullName[1] }
          ],
          fileType: FS.DIRECTORY
        };
      } else if (fileFullName.length == 2 && (newFileFullName.length == 1 || newPath[newPath.length-1] == '..')) {
        newPath.push(newFileFullName[0]);
        message = {
          files: [
            { path: path, wd: global.wd.id, name: fileFullName[0], type: fileFullName[1] },
            { path: newPath, wd: global.wd.id, name: fileFullName[0], type: fileFullName[1] }
          ],
          fileType: FS.FILE
        };
      } else {
        Logger.logMessage(Logger.LOG_WARNING,'Specified file is invalid.');
        displayPrompt();
        break;
      }
      Client.rpc.mv(
        message,
        (error, result) => {
          if (!error) {
              Logger.logMessage(Logger.LOG_INFO,result.message);
          } else {
              Logger.logMessage(Logger.LOG_ERROR,error);
          }
          displayPrompt();
      });
      break;
    case 'help':
      Logger.logMessage(Logger.LOG_INFO,TERMINAL.MESSAGES['HELP']);
      displayHelp();
      displayPrompt();
      break;
    default:
      break;
  }
}

// Split command and arguments and return array data['command','args']
function splitInstruction(data)
{
  data = data.replace(/\n/g, '');
  data = data.split(' ');
  command = data.shift()

  options = [];
  args = [];

  data.forEach( function(item) {
    if (item[0] == '-') {
      item = item.split('');
      item.shift();
      if (item [0] == '-') {
        item.shift();
        options.push(item.join(''));
      } else {
        item.forEach( function(subitem) {
          options.push(subitem);
        });
      }
    } else {
      args.push(item);
    }
  });
  data =
  {
    'command': command,
    'args': args,
    'options': options,
  }
  return data;
}

// get Working dir string
function displayWd()
{
  pathToReturn = JSON.parse(JSON.stringify(global.wd.path));
  if (pathToReturn.length >= 1 && pathToReturn[0] != '/') {
    pathToReturn.unshift('');
  }
  pathToReturn = pathToReturn.join('/');
  return pathToReturn;
}

function displayHelp()
{
  console.log('');
  Object.keys(TERMINAL.COMMANDS).forEach(function(key) {
    console.log('\t'+key+': '+TERMINAL.COMMANDS[key]['desc']+'\n');
    options = Object.keys(TERMINAL.COMMANDS[key]['options']);
    if(options.length > 0){
      console.log('\t\toptions:'+'\n');
    }
    options.forEach( function(subkey) {
      console.log('\t\t\t\t'+'--'+subkey+(TERMINAL.COMMANDS[key]['options'][subkey]['alias'] ? ' -'+TERMINAL.COMMANDS[key]['options'][subkey]['alias'] : '')+' : '+TERMINAL.COMMANDS[key]['options'][subkey]['desc']);
    });
    args = Object.keys(TERMINAL.COMMANDS[key]['args']);
    usageString = '\n\t\tusage: '+key+'';
    listStrings = [];
    args.forEach( function(subkey) {
      usageString += ' <'+subkey+'>';
      listStrings.push('\t\t\t\t'+subkey+': '+TERMINAL.COMMANDS[key]['args'][subkey]);
    });
    console.log(usageString+'\n');
    listStrings.forEach( function(line) {
      console.log(line);
    });
    if (args.length > 0){
      console.log('\n');
    }
  });
}

function displayPrompt()
{
  process.stdout.write(
    TERMINAL.COLORS.Green+User+'@'+Client.address+TERMINAL.COLORS.Blue+':'+displayWd()+TERMINAL.COLORS.Reset+"> "
  );
}

function buildPath(data)
{
  if (data == undefined) {
    return [];
  }
  var path = data.split('/');
  if (data == '/') {
    path = ['/'];
  } else if (data.charAt(0) == '/') {
    path[0] = '/';
  } else if (data != '.' && data != '..'){
    path.unshift('.');
  }
  if (path.indexOf('') != -1) {
    Logger.logMessage(Logger.LOG_ERROR,'Specified path is invalid!');
    return false;
  }
  console.log(path);
  return path;
}

function displayList(docs)
{
  let totalFilesCount = 0;
  if (docs.dirs != undefined) {
    var dirs = docs.dirs.sort(function(a, b) {
      var nameA = a.name.toUpperCase(); // ignore upper and lowercase
      var nameB = b.name.toUpperCase(); // ignore upper and lowercase
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }

      // names must be equal
      return 0;
    });
    totalFilesCount += dirs.length;
  }

  if (docs.fileRefs != undefined) {
    var fileRefs = docs.fileRefs.sort(function(a, b) {
      var nameA = a.name.toUpperCase(); // ignore upper and lowercase
      var nameB = b.name.toUpperCase(); // ignore upper and lowercase
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
      // names must be equal
      return 0;
    });
    totalFilesCount += fileRefs.length;
  }

  console.log('total '+(totalFilesCount));
  if (dirs != undefined) dirs.forEach(function(item){
    item.metadata = JSON.parse(item.metadata);
    console.log('d'+item.permissions+' '+item.owner+' '+item.group+' '+formatDate(item.metadata.creationDate)+' '+item.name);
  });
  if (fileRefs != undefined) fileRefs.forEach(function(item){
    item.metadata = JSON.parse(item.metadata);
    var file = docs.files.find(file => {
      return file.id == item.ref;
    });
    console.log('-'+item.permissions+' '+item.owner+' '+item.group+' '+formatDate(item.metadata.creationDate)+' '+item.name+'.'+file.type);
  });
  return;
}

function formatDate(date){
  dateArray = date.split('T');
  timeArray = dateArray[1].split(':');
  time = timeArray[0]+':'+timeArray[1];
  dateArray = dateArray[0].split('-');
  month = dateArray[1][0] == 0 ? DATES.MONTHS[dateArray[1][1]] : DATES.MONTHS[dateArray[1]];
  day = dateArray[2][0] == 0 ? ''+dateArray[2][1] : dateArray[2];
  return month+' '+day+' '+time;
}
