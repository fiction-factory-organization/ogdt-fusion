/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-06-29T09:34:35+02:00
 * @Project: OGDT Fusion
 * @Filename: client.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-07-10T02:37:04+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */


const address = 'localhost'

const grpc = require('grpc')
const protoLoader = require('@grpc/proto-loader');

const fsProtoFile = protoLoader.loadSync('../proto/fs.proto');
const fsProto = grpc.loadPackageDefinition(fsProtoFile);
const rpc = new fsProto.FileSystemService(address+':50051',grpc.credentials.createInsecure())

var wdId = undefined;

module.exports =
{
  rpc: rpc,
  wdId: wdId,
  address: address,
}
