/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-06-29T09:34:35+02:00
 * @Project: OGDT Fusion
 * @Filename: const.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-06T10:55:47+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

const TERMINAL = {

  // COLORS
  COLORS: {
    Reset:        '\x1b[0m',
    Blue:         '\x1b[1;34m',
    Green:        '\x1b[1;32m',
  },

  // Commands Consts
  COMMANDS:
    {
      'cd':
        {
          'desc':'Enter the specified directory',
          'options': {},
          'args':
            {
              'path' : 'Path of the directory to browse',
            },
        },
      'ls':
        {
          'desc':'Display content of the specified directory',
          'options':
            {
              'raw':
                {
                  'desc':'display raw format (JSON)',
                  'alias':'r',
                },
              'list':
                {
                  'desc':'display as vertical list',
                  'alias':'l',
                },
            },
          'args':
            {
              'path' : 'Path of the directory\'s content to list',
            },
        },
      'mv':
        {
          'desc':'Move a specified file or directory to a new location',
          'options': {},
          'args':
            {
              'path' : 'Path of the file or directory to move',
              'newpath' : 'Path of the new parent directory',
            },
        },
      'rm':
        {
          'desc':'Delete a specified file or directory',
          'options': {},
          'args':
            {
              'path' : 'Path of the file or directory to remove',
            },
        },
      'mkdir':
        {
          'desc':'Create a new directory',
          'options': {},
          'args':
            {
              'path' : 'Path of the new directory',
            },
        },
      'touch':
        {
          'desc':'Create a new file',
          'options': {},
          'args':
            {
              'path' : 'Path of the new file',
            },
        },
    },

  // Message Consts
  MESSAGES:
    {
      'TMA':'Too many arguments',
      'UNKNOWN':'Unknown command',
      'HELP':'Fusion terminal usage:',
    },

  CD_MESSAGES:
    {
      'TFA':'cd command requires a directory path',
    },

  LS_MESSAGES:
    {
      'TFA':'ls command requires a directory path or a file name',
    },

  TOUCH_MESSAGES:
    {
      'TFA':'touch command requires a file name',
    },

  MKDIR_MESSAGES:
    {
      'TFA':'mkdir command requires a directory path',
    },

  RM_MESSAGES:
    {
      'TFA':'rm command requires a file path',
    },

  MV_MESSAGES:
    {
      'TFA':'mv command requires a file path and a new location',
    },

  RMDIR_MESSAGES:
    {
      'TFA':'rmdir command requires a directory path',
    },

}

const FS =
  {
    DIRECTORY: 0,
    FILE: 1,
  }

const DATES =
  {
    MONTHS:
      {
        1: 'Jan',
        2: 'Feb',
        3: 'Mar',
        4: 'Apr',
        5: 'May',
        6: 'Jun',
        7: 'Jul',
        8: 'Aug',
        9: 'Sep',
        10: 'Oct',
        11: 'Nov',
        12: 'Dec',
      }
  }


module.exports = {
    TERMINAL: TERMINAL,
    FS: FS,
    DATES: DATES,
};
