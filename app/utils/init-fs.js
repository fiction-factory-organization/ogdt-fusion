/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-07-01T11:57:08+02:00
 * @Project: OGDT Fusion
 * @Filename: init-fs.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-07-03T02:08:49+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */



const Logger = require('../commons/logger');
const Client = require('./client');
const async = require('async');

Logger.logMessage(Logger.LOG_INFO,'Creating Root Directory / ...');

let dir = { name: '/', path: ['INIT_CODE'], owner: 'fusion', group: 'fusion', permissions: '' };
if (process.argv[2] == '--force-reset') {
  dir.path = ['RESET_CODE'];
}

Client.rpc.mkdir(dir, (error, files) => {
    if (!error) {
        Logger.logMessage(Logger.LOG_INFO,'Root directory registered!');
        Logger.logMessage(Logger.LOG_INFO,'File system successfully initialized! Exiting...');
    } else {
        Logger.logMessage(Logger.LOG_ERROR,error.message);
        Logger.logMessage(Logger.LOG_ERROR,'Failed to register root dir! Please try again by adding "--force-reset"');
        process.exit();
    }
});
