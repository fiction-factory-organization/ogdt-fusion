/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-06-29T09:34:35+02:00
 * @Project: OGDT Fusion
 * @Filename: index.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-06T14:08:50+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

global.grpc = require('grpc');
const util = require('util');
const protoLoader = require('@grpc/proto-loader');
const Logger = require('./commons/logger');
global.Mongoose = require('mongoose');

// CONSTs

const MONGO_ADDRESS = 'docker';
const MONGO_USER = 'root';
const MONGO_PASSWORD = 'root';

const CONST = require('./utils/const');
global.FS = CONST.FS;
const fsServer = require('./fileSystemServer');

// Entrypoint message
Logger.logMessage(Logger.LOG_INFO, "Fusion server is starting...");

// Mongoose init and FS init db controller
Mongoose.connect('mongodb://'+MONGO_USER+':'+MONGO_PASSWORD+'@'+MONGO_ADDRESS+'/fusion-fs', { useNewUrlParser: true });

// Error handlers
Mongoose.connection.on("open", function(ref) {
  Logger.logMessage(Logger.LOG_INFO, "Connected to mongo server!");
  Server.bind('0.0.0.0:50051', grpc.ServerCredentials.createInsecure());
  Server.start();
  return Logger.logMessage(Logger.LOG_INFO, 'Fusion server running on port: 0.0.0.0:50051');
});

Mongoose.connection.on("error", function(err) {
  Logger.logMessage(Logger.LOG_ERROR, "Could not connect to mongo server!");
  Logger.logMessage(Logger.LOG_ERROR, err.message);
  Logger.logMessage(Logger.LOG_INFO, "Fusion server closing...")
  return process.exit(0);
});

// gRPC calls to proto def
const ProtoOptions = {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true
}
const fsProtoFile = protoLoader.loadSync('./proto/fs.proto',ProtoOptions);
const fsProto = grpc.loadPackageDefinition(fsProtoFile);

global.Server = new grpc.Server();
Server.addService(fsProto.FileSystemService.service, {

  // List Dir content
  async Ls(call, callback) {
    fsServer.ls(call, callback);
  },

  async Cd(call, callback) {
    fsServer.cd(call, callback);
  },

  async Touch(call, callback) {
    fsServer.touch(call, callback);
  },

  async Mv(call, callback) {
    fsServer.mv(call, callback);
  },

  async Mkdir(call, callback) {
    fsServer.mkdir(call, callback);
  },

  async Rm(call, callback){
    fsServer.rm(call, callback);
  },

  async Rmdir(call, callback){
    fsServer.rmdir(call, callback);
  },

  // SaveFile(call, callback){
  //
  // },
})

process.on('SIGINT', () => {
  Logger.logMessage(Logger.LOG_INFO, "Fusion server closing...")
  process.exit(0);
});
