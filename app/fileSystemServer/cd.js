/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-08-05T16:55:04+02:00
 * @Project: OGDT
 * @Filename: cd.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-05T17:36:29+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

const FSdef = require('../db/fs.js')
const FileSystem = FSdef.FileSystem;

const utils = require('./utils/utils');

async function cd(call, callback){
  const { path, wd } = call.request;
  promises = utils.browsePath(path, wd, call, callback);

  dir = {}

  await promises.then(async function (dir) {
    pathToSend = '';
    promise = utils.getAbsolutePath(dir._id, call, callback);

    await promise.then(function (path){
      if (dir.name != '/') {
        path.shift();
        path.push(dir.name);
      }
      pathToSend = path;
    });
    return callback(null, {path: pathToSend, wd: dir._id});
  });
}

module.exports = cd;
