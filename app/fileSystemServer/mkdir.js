/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-08-05T17:08:29+02:00
 * @Project: OGDT
 * @Filename: mkdir.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-10T12:58:20+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */


const FSdef = require('../db/fs.js')
const FileSystem = FSdef.FileSystem;

const utils = require('./utils/utils');

async function mkdir(call, callback){
  const { name, path, wd, owner, group, permissions} = call.request
  if (name == '/') {
    switch (path[0]) {
      case 'INIT_CODE':
        var fs = new FileSystem({
          criteria: {
            name: '/',
          },
          projections: {
          },
          options: {
            lean: true,
          },
        });
        fs.getDir(function (err, docs) {
          if (docs.length != 0) {
            return callback({
              code: 409,
              message: "Root dir already exist!",
              status: grpc.status.ALREADY_EXISTS
            });
          }
          fs = new FileSystem({
             name: '/',
             owner: owner != '' ? owner : DEFAULT_OWNER,
             group: group != '' ? group : DEFAULT_GROUP,
             permissions: permissions != '' ? permissions : DEFAULT_PERMISSIONS,
          });
          fs.createDir(callback);
        });
        break;
      case 'RESET_CODE':
        var fs = new FileSystem({});
        fs.resetHierarchy(callback);
        fs = new FileSystem({
           name: '/',
           owner: owner != '' ? owner : DEFAULT_OWNER,
           group: group != '' ? group : DEFAULT_GROUP,
           permissions: permissions != '' ? permissions : DEFAULT_PERMISSIONS,
        });
        fs.createDir(callback);
        break;
    }
  } else {

    // Browse path to find dir
    promises = utils.browsePath(path, wd, call, callback);

    await promises.then( async function(dir) {

      // Test if dir already exists
      promise = utils.dirExist(dir, name, call, callback);

      await promise.then(
        async function(result) {
          // return if error
          if ( result != false){
            return;
          }

          // Create new dir with new id and add this id to dir children
          var newId = Mongoose.Types.ObjectId();
          dir.children.push(newId);

          // Save new dir
          fs = new FileSystem({
             _id: newId,
             name: name,
             owner: owner != '' ? owner : DEFAULT_OWNER,
             group: group != '' ? group : DEFAULT_GROUP,
             permissions: permissions != '' ? permissions : DEFAULT_PERMISSIONS,
          });
          await fs.createDir();

          // Save new children in Parent Directory
          fs = new FileSystem(dir);
          await fs.updateDir(callback);
          return callback(null, { message: 'New directory successfully created!' })
        },
        function(reason) {
          console.log(reason);
          return callback(reason);
        }
      );
    });
  }
}

module.exports = mkdir;
