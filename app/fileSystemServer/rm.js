/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-08-05T17:09:38+02:00
 * @Project: OGDT
 * @Filename: rm.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-10T13:36:16+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */


const FSdef = require('../db/fs.js')
const FileSystem = FSdef.FileSystem;

const utils = require('./utils/utils');

async function rm(call, callback){
  const { path, wd, name, type } = call.request;
  // Browse path to find Dir
  promises = utils.browsePath(path, wd, call, callback);

  await promises.then( async function (parentDir) {

    promise = utils.getChildFileAndRef(parentDir.files, type, name, call, callback);

    await promise.then(
      async function(result) {
        file = result[1];
        fileRef = result[0];

        // Delete fileRef
        payload = {
           criteria: {
              _id: fileRef._id,
           },
           projections: {
              __v: 0
           },
           options: {
              lean: true
           }
        }
        let fs = new FileSystem(payload);
        cleanUpPromise = new Promise(function(resolve, reject){
          fs.deleteFileRef(function(err,docs){
            if (err) {
              return callback({
                code: 500,
                message: "Failed to delete fileRef!",
                status: grpc.status.INTERNAL
              });
            }
            resolve();
          });
        });

        await cleanUpPromise.then( async function () {
          // Parse to string to avoid indexOf fail due to type differences
          filesIds = parentDir.files.map(function(item){return String(item);});

          // Remove Id from parentDir children
          index = filesIds.indexOf(String(fileRef._id));
          if (index > -1) {
            parentDir.files.splice(index, 1);
          } else {
            return callback({
              code: 500,
              message: "Hierarchy fatal error!",
              status: grpc.status.INTERNAL
            });
          }

          // Remove from children
          fs = new FileSystem(parentDir);
          await fs.updateDir();

          // Verify for fileRef existence
          fs = new FileSystem({
            criteria: {
               ref: file._id,
            },
            projections: {
               __v: 0
            },
            options: {
            }
          });

          // Get fileRef from reference Id
          cleanUpOriginFilePromise = new Promise( async function (resolve,reject){
            fs.getFileRef( async function (err, docs) {
              if (err) {
                reject({
                 code: 500,
                 status: grpc.status.INTERNAL
                });
              }
              if (docs && docs.length == 0) {
                // Delete file if no fileRef left
                fs = new FileSystem({
                  criteria: {
                     _id: file._id,
                  },
                  projections: {
                     __v: 0
                  },
                  options: {
                  }
                });
                fs.deleteFile(function (err, docs) {
                  if (err) {
                    console.log(err);
                    return callback({
                      code: 500,
                      status: grpc.status.INTERNAL
                    })
                  } else {
                    return callback(null,{message: "Successfully deleted fileRef! Moreover, origin file was deleted as no fileRef was referencing it anymore."});
                  }
                });
              } else {
                resolve({
                  code: 200,
                  message: "Successfully deleted fileRef!",
                  status: grpc.status.OK
                });
              }
            });
          });
        });

      },
      function(reason) {
        return callback(reason);
      }
    );
  });
}

module.exports = rm;
