/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-08-05T17:11:02+02:00
 * @Project: OGDT
 * @Filename: rmdir.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-05T17:19:48+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */


const FSdef = require('../db/fs.js')
const FileSystem = FSdef.FileSystem;

const utils = require('./utils/utils');

async function rmdir(call, callback){
  const { path, wd } = call.request;
  // Browse path to find Dir
  promises = utils.browsePath(path, wd, call, callback);

  await promises.then( async function (dir) {

    if (dir.children.length != 0 || dir.files.length != 0){
      return callback({
        code: 400,
        message: "Specified dir contains files!",
        status: grpc.status.FAILED_PRECONDITION
      });
    }

    promise = utils.getDirFromChild(dir._id, call, callback);

    await promise.then(
      function(parentDir) {
        index = parentDir.children.indexOf(dir._id);
        if (index > -1) {
          parentDir.children.splice(index, 1);
        } else {
          return callback({
            code: 500,
            message: "Hierarchy fatal error!",
            status: grpc.status.INTERNAL
          });
        }

        // Delete dir
        payload = {
           criteria: {
              _id: dir._id,
           },
           projections: {
              __v: 0
           },
           options: {
              lean: true
           }
        }
        let fs = new FileSystem(payload);
        fs.deleteDir();

        // Remove children
        fs = new FileSystem(parentDir);
        fs.updateDir(callback);
      },
      function(reason) {
        return callback(reason);
      }
    );
  });
}

module.exports = rmdir;
