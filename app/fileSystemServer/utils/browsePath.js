/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-08-05T16:44:57+02:00
 * @Project: OGDT
 * @Filename: browsePath.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-06T16:50:56+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

const FSdef = require('../../db/fs.js')
const FileSystem = FSdef.FileSystem;

const getDirFromChild = require('./getDirFromChild');

async function browsePath(path, wd, call, callback, directReturnOnFail = true) {

 // If no path provided
 if (path.length == 0) {
   payload = {
      criteria: {
         _id: wd,
      },
      projections: {
         __v: 0
      },
      options: {
         lean: true
      }
   }
   fs = new FileSystem(payload);
   await fs.getDir(function (err,docs) {
     if (err || docs && docs.length == 0) {
       console.log(err);
       return callback({
         code: 500,
         status: grpc.status.INTERNAL
       });
     }
     doc = docs[0];
   });
   return doc;
 }

 // init initial ID
 doc = {};
 doc._id = wd;

 // promise for each path element
 return path.reduce((promiseChain, item) => {

   return promiseChain.then( async (docToReturn) => {
     // payload for current element

     payload = {
        criteria: {
           name: item,
        },
        projections: {
           __v: 0
        },
        options: {
           lean: true
        }
     }
     if (item == '.') {
       payload.criteria = { _id: docToReturn._id };
     }
     if (item == '..') {
       promise = getDirFromChild(docToReturn._id, call, callback);

       await promise.then(
         function(parentDir) {
           payload.criteria = { _id: parentDir._id };
         },
         async function(reason) {
           payload = {
              criteria: {
                 name: '/',
              },
              projections: {
                 __v: 0
              },
              options: {
                 lean: true
              }
           }
           fs = new FileSystem(payload);
           await fs.getDir(function (err,docs) {
             if (err || docs && docs.length == 0) {
               console.log(err);
               return callback({
                 code: 500,
                 status: grpc.status.INTERNAL
               });
             }
             doc = docs[0];
           });
           return doc;
         }
       );
     }



     // Check if previous dir has children
     if (doc.children && doc.children.length != 0) {
       payload.criteria._id = { $in: doc.children };
     }

     fs = new FileSystem(payload);
     var mongoosePromise = fs.getDir(function (err, docs) {
       if (err) {
         console.log(err);
         return callback({
           code: 500,
           status: grpc.status.INTERNAL
         });
       }
       if (docs && docs.length == 0 && directReturnOnFail == true) {
         return callback({
           code: 400,
           message: "Specified dir does not exist!",
           status: grpc.status.FAILED_PRECONDITION
         });
       }
     }).exec();

     docToReturn = {};

     await mongoosePromise.then(function(docs) {
       docToReturn = docs[0];
     });

     return docToReturn;

   }).catch(console.error);

 }, Promise.resolve({_id: wd}));
}

module.exports = browsePath;
