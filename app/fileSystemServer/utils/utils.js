/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-08-05T16:48:07+02:00
 * @Project: OGDT
 * @Filename: utils.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-06T12:49:09+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

const browsePath = require('./browsePath');
const dirExist = require('./dirExist');
const fileRefExist = require('./fileRefExist');
const getAbsolutePath = require('./getAbsolutePath');
const getChildFileAndRef = require('./getChildFileAndRef');
const getDirFromChild = require('./getDirFromChild');

module.exports = {
  browsePath: browsePath,
  dirExist: dirExist,
  fileRefExist: fileRefExist,
  getAbsolutePath: getAbsolutePath,
  getChildFileAndRef: getChildFileAndRef,
  getDirFromChild: getDirFromChild,
}
