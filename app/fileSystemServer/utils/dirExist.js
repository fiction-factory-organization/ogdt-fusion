/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-08-05T16:57:32+02:00
 * @Project: OGDT
 * @Filename: dirExist.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-06T14:22:14+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

const FSdef = require('../../db/fs.js')
const FileSystem = FSdef.FileSystem;

function dirExist(dir, name, call, callback){

 promise = new Promise( function (resolve, reject) {
   if (dir.children.length > 0) {
     fs = new FileSystem({
        criteria: {
           _id: { $in: dir.children},
           name: name,
        },
        projections: {
           __v: 0
        },
        options: {
        }
     });
     fs.getDir(function (err, docs) {
       if (err) {
         console.log(err);
         reject({
           code: 500,
           status: grpc.status.INTERNAL
         })
       }
       if (docs && docs.length > 0) {
         reject({
           code: 409,
           message: "This directory already exists!",
           status: grpc.status.ALREADY_EXISTS
         })
       }
       resolve(false);
     });
   } else {
     resolve(false);
   }
   });
 return promise;
}

module.exports = dirExist;
