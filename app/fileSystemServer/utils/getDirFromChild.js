/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-08-05T16:59:47+02:00
 * @Project: OGDT
 * @Filename: getDirFromChild.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-06T09:58:04+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

const FSdef = require('../../db/fs.js')
const FileSystem = FSdef.FileSystem;

// return Parent dir for a given Child Id
function getDirFromChild(id, call, callback){

 promise = new Promise( function (resolve, reject) {
   fs = new FileSystem({
      criteria: {
         children: { $elemMatch: { $eq: Mongoose.Types.ObjectId(id) }},
      },
      projections: {
         __v: 0
      },
      options: {
      }
   });
   fs.getDir(function (err, docs) {
     if (err) {
       console.log(err);
       reject({
         code: 500,
         status: grpc.status.INTERNAL
       })
     }
     if (docs && docs.length == 0) {
       reject({
         code: 500,
         message: "Failed to fetch parent dir!",
         status: grpc.status.INTERNAL
       });
     }
     resolve(docs[0]);
   });
 });
 return promise;
}

module.exports = getDirFromChild;
