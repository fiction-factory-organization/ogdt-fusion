/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-08-05T16:58:38+02:00
 * @Project: OGDT
 * @Filename: fileRefExist.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-06T11:16:28+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

const FSdef = require('../../db/fs.js')
const FileSystem = FSdef.FileSystem;

const utils = require('./utils');

function fileRefExist(dir, name, type, call, callback){

 promise = new Promise( function (resolve, reject) {
   if (dir.files.length > 0) {
     fs = new FileSystem({
        criteria: {
           _id: { $in: dir.files},
           name: name,
           type: type,
        },
        projections: {
           __v: 0
        },
        options: {
        }
     });
     fs.getFile(function (err, docs) {
       if (err) {
         console.log(err);
         reject({
           code: 500,
           status: grpc.status.INTERNAL
         })
       }
       if (docs && docs.length > 0) {
         reject({
           code: 409,
           message: "This file already exists!",
           status: grpc.status.ALREADY_EXISTS
         })
       }
       resolve(false);
     });
   } else {
     resolve(false);
   }
   });
 return promise;
}

module.exports = fileRefExist;
