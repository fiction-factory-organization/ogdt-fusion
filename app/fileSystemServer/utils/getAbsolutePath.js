/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-08-05T16:53:43+02:00
 * @Project: OGDT
 * @Filename: getAbsolutePath.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-06T09:57:57+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

const FSdef = require('../../db/fs.js')
const FileSystem = FSdef.FileSystem;

const getDirFromChild = require('./getDirFromChild');

function getAbsolutePath(id, call, callback){

 promise = new Promise( async function (resolve, reject) {

   done = false;
   dirId = id;

   const fetchPath = function(path) {
     return new Promise( async (resolve, reject) => {
       if (path[0] == '/') {
         resolve(path);
       } else {
         promise = getDirFromChild(dirId, call, callback);

         await promise.then(
           function(parentDir) {
             dirId = parentDir._id;
             path.unshift(parentDir.name);
           },
           function(reason) {
             path.push('/');
           }
         );
         resolve(fetchPath(path));
       }
     });
   }

   fetchPath([]).then(data => {
     resolve(data);
   });
 });
 return promise;
}

module.exports = getAbsolutePath;
