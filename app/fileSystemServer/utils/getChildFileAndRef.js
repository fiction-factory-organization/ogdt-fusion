/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-08-05T17:00:52+02:00
 * @Project: OGDT
 * @Filename: getChildFileAndRef.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-10T12:46:40+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

const FSdef = require('../../db/fs.js')
const FileSystem = FSdef.FileSystem;

// Return array[fileRef,file];
function getChildFileAndRef(fileRefsId, type, name, call, callback){

  promise = new Promise( function (resolve, reject) {
    fs = new FileSystem({
      criteria: {
         _id: { $in: fileRefsId },
         name: name,
      },
      projections: {
         __v: 0
      },
      options: {
      }
    });

    // Get hierarchycal fileRef from Id and Child id list
    fs.getFileRef(function (err, docs) {
      if (err) {
       console.log(err);
       return reject({
         code: 500,
         status: grpc.status.INTERNAL
       })
      }
      if (docs && docs.length == 0) {
       return reject({
         code: 500,
         message: "Failed to fetch child fileRef!",
         status: grpc.status.INTERNAL
       });
      }
      fileRef = docs[0]

      fs = new FileSystem({
        criteria: {
           _id: fileRef.ref,
           type: type,
        },
        projections: {
           __v: 0
        },
        options: {
        }
      });

      // Get file from fileRef reference Id
      fs.getFile(function (err, docs) {
        if (err) {
         console.log(err);
         return reject({
           code: 500,
           status: grpc.status.INTERNAL
         })
        }
        if (docs && docs.length == 0) {
         return reject({
           code: 500,
           message: "Failed to fetch file!",
           status: grpc.status.INTERNAL
         });
        }
        file = docs[0];
        return resolve([fileRef,file]);
      });
    });
  });
  return promise;
}

module.exports = getChildFileAndRef;
