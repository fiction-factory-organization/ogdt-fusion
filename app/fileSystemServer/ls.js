/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-08-05T16:30:07+02:00
 * @Project: OGDT
 * @Filename: ls.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-06T12:16:55+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

const FSdef = require('../db/fs.js')
const FileSystem = FSdef.FileSystem;

const utils = require('./utils/utils');

async function ls(call, callback){
  const { path, wd } = call.request;
  // console.log(wd);
  promises = utils.browsePath(path, wd, call, callback);

  // ID lists
  dirsId = [];
  fileRefsId = [];

  // populate children and files array
  promises.then( async function (dir) {
    if (!dir) {
      return;
    }
    if (!dir.children && !dir.files || dir.children.length == 0 && dir.files.length == 0) {
      return callback({
        code: 409,
        message: "Specified dir is empty",
        status: grpc.status.ABORTED
      });
    }
    dirsId = dir.children;
    fileRefsId = dir.files;

    dirDocs = [];
    fileDocs = [];
    fileRefDocs = [];

    fetchPromises = [];

    // get dirs
    let fs = new FileSystem({
      criteria: {
        _id: { $in: dirsId },
      },
      projections: {
      },
      options: {
        lean: true,
      },
    });

    fetchPromises.push(new Promise( function(resolve,reject) {
      fs.getDir(function (err, docs) {
        if(err){
          return callback({
            code: 409,
            message: "Unable to retrieve Dirs",
            status: grpc.status.INTERNAL
          });
        }
        resolve(docs);
      });
    }));

    // get fileRefs
    fs = new FileSystem({
      criteria: {
        _id: { $in: fileRefsId },
      },
      projections: {
      },
      options: {
        lean: true,
      },
    });

    fetchPromises.push(new Promise( function(resolve,reject) {
      fs.getFileRef(function (err, docs) {
        if(err){
          return callback({
            code: 409,
            message: "Unable to retrieve fileRefs",
            status: grpc.status.INTERNAL
          });
        }
        filesId = [];
        fileRefDocs = docs;
        docs.forEach((item) => {
          filesId.push(item.ref);
        })
        // get files
        fs = new FileSystem({
          criteria: {
            _id: { $in: filesId },
          },
          projections: {
          },
          options: {
            lean: true,
          },
        });

        fs.getFile(function (err, docs) {
          if(err){
            return callback({
              code: 409,
              message: "Unable to retrieve files",
              status: grpc.status.INTERNAL
            });
          }
          resolve([fileRefDocs,docs]);
        });
      });
    }));

    Promise.all(fetchPromises).then(function(results) {
      fileRefDocs = results[1][0];
      fileDocs = results[1][1];
      dirDocs = results[0];

      dirs = [];
      files = [];
      fileRefs = [];

      fileRefDocs.forEach(function(fileRef) {
        fileRefs.push({
          id: fileRef._id,
          ref: fileRef.ref,
          name: fileRef.name,
          permissions: fileRef.permissions,
          owner: fileRef.owner,
          group: fileRef.group,
          metadata: JSON.stringify(fileRef.metadata),
        });
      });

      fileDocs.forEach(function(file) {
        files.push({
          id: file._id,
          type: file.type,
          metadata: JSON.stringify(file.metadata),
        });
      });

      dirDocs.forEach(function(dir) {
        dirs.push({
          id: dir._id,
          name: dir.name,
          permissions: dir.permissions,
          owner: dir.owner,
          group: dir.group,
          metadata: JSON.stringify(dir.metadata),
        });
      });

      return callback(null, {
        dirs: dirs,
        files: files,
        fileRefs: fileRefs,
      });
    });

  });

}

module.exports = ls;
