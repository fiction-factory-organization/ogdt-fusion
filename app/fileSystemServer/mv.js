/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-08-05T17:07:17+02:00
 * @Project: OGDT
 * @Filename: mv.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-06T16:57:17+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */


const FSdef = require('../db/fs.js')
const FileSystem = FSdef.FileSystem;

const utils = require('./utils/utils');

async function mv(call, callback){
  const { files, fileType } = call.request;
  parentDirPromises = utils.browsePath(files[0].path, files[0].wd, call, callback);
  newParentDirPromises = utils.browsePath(files[1].path, files[1].wd, call, callback, false);
  // Getting newDir
  var oldDir = undefined;
  await parentDirPromises.then( async (dir) => {
    oldDir = dir;
  });
  if (oldDir == undefined){
    return;
  }
  // return callback(null, {message: 'Test!'});

  switch (fileType) {
    case FS.DIRECTORY:
      await newParentDirPromises.then( async function (newParentDir){

        if (newParentDir == undefined) {
          files[1].name = files[1].path.pop();
          previousInPathPromise = utils.browsePath(files[1].path, files[1].wd, call, callback);
          await previousInPathPromise.then(
            async function (previousDir) {
              newParentDir = previousDir;
            }
          )
        } else {
          files[1].name = files[0].name;
        }
        // Check presence of similar dir in new parent dir
        promise = utils.dirExist(newParentDir, files[1].name, call, callback);

        await promise.then(
          async function (result) {
            // return if error
            // console.log(result);
            if (result != false){
              return;
            }

            // get old Parent dir from moving dir
            promise = utils.getDirFromChild(oldDir._id, call, callback);

            await promise.then(
              async function(parentDir) {
                index = parentDir.children.indexOf(oldDir._id);
                if (index > -1) {
                  if (String(parentDir._id) != String(newParentDir._id)){
                    parentDir.children.splice(index, 1);
                    newParentDir.children.push(oldDir._id);
                  }
                } else {
                  return callback({
                    code: 500,
                    message: "Hierarchy fatal error!",
                    status: grpc.status.INTERNAL
                  });
                }
                // console.log(files[0],files[1]);
                // return callback(null, {message: 'Test!'});

                if (files[0].name != files[1].name) {
                  //Change dir name
                  oldDir.name = files[1].name;
                  fs = new FileSystem(oldDir);
                  fs.updateDir(callback);
                }

                if (String(parentDir._id) != String(newParentDir._id)){
                  // Save new children in Parent Directory
                  fs = new FileSystem(newParentDir);
                  await fs.updateDir( async function (err,docs) {
                    // Remove children from Parent Directory
                    if (err) {
                        return callback({
                          code: 500,
                          message: "Failed to save new relationship!",
                          status: grpc.status.INTERNAL
                        })
                    }
                    fs = new FileSystem(parentDir);
                    await fs.updateDir( function (err,docs) {
                      if (err) {
                          return callback({
                            code: 500,
                            message: "Failed to remove old relationship!",
                            status: grpc.status.INTERNAL
                          });
                      }
                    });
                  });
                  return callback(null, {message: 'Dir has been successfully moved!'});
                } else {
                  return callback(null, {message: 'Dir has been successfully moved!'});
                }
              }
            );
          },
          function(reason) {
            return callback(reason);
          }
        );
      });
      break;
    case FS.FILE:
      await newParentDirPromises.then( async function (newParentDir){

        // Check presence of similar dir in new parent dir
        promise = utils.fileRefExist(newParentDir, files[1].name, files[1].type, call, callback);

        await promise.then(
          async function(result) {
            // return if error
            if ( result != false){
              return;
            }

            filePromise = utils.getChildFileAndRef(oldDir.files, files[0].type, files[0].name, call, callback);

            await promise.then(
              async function(result) {
                file = result[1];
                fileRef = result[0];

                // Parse to string to avoid indexOf fail due to type differences
                filesIds = oldDir.files.map(function(item){return String(item);});

                // Check index and remove Id from oldDir
                index = filesIds.indexOf(String(fileRef._id));
                if (index > -1) {
                  if (String(oldDir._id) != String(newParentDir._id)){
                    oldDir.files.splice(index, 1);
                    newParentDir.files.push(fileRef._id);
                  }
                } else {
                  return callback({
                    code: 500,
                    message: "Hierarchy fatal error!",
                    status: grpc.status.INTERNAL
                  });
                }

                if (files[0].name != files[1].name) {
                  //Change file name
                  fileRef.name = files[1].name;
                  console.log(fileRef);
                  fs = new FileSystem(fileRef);
                  fs.updateFileRef(callback);
                }

                if (String(oldDir._id) != String(newParentDir._id)){
                  // Save new children in Parent Directory
                  fs = new FileSystem(newParentDir);
                  await fs.updateDir( async function (err,docs) {
                    // Remove children from Parent Directory
                    if (err) {
                        return callback({
                          code: 500,
                          message: "Failed to save new relationship!",
                          status: grpc.status.INTERNAL
                        })
                    }
                    fs = new FileSystem(oldDir);
                    await fs.updateDir( function (err,docs) {
                      if (err) {
                          return callback({
                            code: 500,
                            message: "Failed to remove old relationship!",
                            status: grpc.status.INTERNAL
                          });
                      }
                    });
                    return callback(null, {message: 'File has been successfully moved!'});
                  });
                } else {
                  return callback(null, {message: 'File has been successfully moved!'});
                }
              },
              function(reason) {
                return callback(reason);
              }
            );

          },
          function(reason) {
            return callback(reason);
          }
        );
      });
      break;
  }
}

// await promises.then( async function (dir){
//
//     // Test if file already exists
//     promise = fileRefExist(dir, name, type, call, callback);
//
//     await promise.then(
//       async function(result) {
//         // return if error
//         if ( result != false){
//           return;
//         }
//
//         // Create new file with new id and add this id to dir children
//         var newId = Mongoose.Types.ObjectId();
//         dir.files.push(newId);
//
//         // Save new file
//         fs = new FileSystem({
//            _id: newId,
//            type: type,
//            name: name,
//            owner: owner != '' ? owner : DEFAULT_OWNER,
//            group: group != '' ? group : DEFAULT_GROUP,
//            permissions: permissions != '' ? permissions : DEFAULT_PERMISSIONS,
//         });
//         await fs.createFile();
//
//         // Save new children in Parent Directory
//         fs = new FileSystem(dir);
//         await fs.updateDir(callback);
//         return callback(null, {message: 'New file successfully created!'})
//       },
//       function(reason) {
//         return callback(reason);
//       }
//     );
// });

module.exports = mv;
