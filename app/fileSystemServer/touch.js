/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-08-05T17:02:14+02:00
 * @Project: OGDT
 * @Filename: touch.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-06T11:19:02+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */

const FSdef = require('../db/fs.js')
const FileSystem = FSdef.FileSystem;

const utils = require('./utils/utils');

async function touch(call, callback){
  const { name, path, wd, owner, group, permissions, workgroup, project, type } = call.request;
  promises = utils.browsePath(path, wd, call, callback);

  await promises.then( async function (dir){

      // Test if fileRef already exists
      promise = utils.fileRefExist(dir, name, type, call, callback);

      await promise.then(
        async function(result) {
          // return if error
          if ( result != false){
            return;
          }

          // Create new file with new id, new fileRef with new refId and add refId to dir children
          var newId = Mongoose.Types.ObjectId();
          var newRefId = Mongoose.Types.ObjectId();
          dir.files.push(newRefId);

          // Save new file
          fs = new FileSystem({
             _id: newId,
             type: type,
          });
          await fs.createFile();

          // Save new fileRef
          fs = new FileSystem({
             _id: newRefId,
             ref: newId,
             name: name,
             owner: owner != '' ? owner : DEFAULT_OWNER,
             group: group != '' ? group : DEFAULT_GROUP,
             permissions: permissions != '' ? permissions : DEFAULT_PERMISSIONS,
          });
          await fs.createFileRef();

          // Save new children in Parent Directory
          fs = new FileSystem(dir);
          await fs.updateDir(callback);
          return callback(null, {message: 'New file successfully created!'})
        },
        function(reason) {
          return callback(reason);
        }
      );
  });
}

module.exports = touch;
