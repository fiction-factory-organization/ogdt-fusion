/**
 * @Author: Thibaud FAURIE
 * @Date:   2019-06-29T09:35:43+02:00
 * @Project: OGDT Fusion
 * @Filename: delta.js
 * @Last modified by:   Thibaud FAURIE
 * @Last modified time: 2019-08-06T10:58:55+02:00
 * @License: GNU GPLv3
 * @Copyright: Copyright (C) 2019 Thibaud FAURIE

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.
 */



const Schema = Mongoose.Schema;
var ObjectId = Mongoose.Schema.Types.ObjectId;
var Metadata = require('./metadata');

let delta = new Schema({
    "ref" : {
      type: [ObjectId], // Node Id
      required:true
    },
    "message" : {
      type: String,
      required:true
    },
    "date" : {
      type: Date,
      required:true
    },
    "diff" : {
      type: String,
      required:true
    },
    "metadata" : {
     type: Metadata,
     default: Metadata,
    },
}, {strict: true});

module.exports = Mongoose.model('deltas', delta);
